import './About.css';
import valentinImg from '../../assets/images/climb.png';
import { FaReact, FaJs, FaHtml5, FaCss3, FaGitlab, FaDumbbell, FaMountain, FaSuitcase, FaFutbol } from 'react-icons/fa';
import { SiChakraui } from 'react-icons/si';
import {TbBrandTypescript} from 'react-icons/tb'
import { useState } from 'react';

function About() {
  const [activeMenu, setActiveMenu] = useState("tech");
  
  return (
    <div className="about-me-container">
      <div className="about-me-content">
        <div className="about-me-left">
          <img src={valentinImg} alt="Valentin Georgiev" className="profile-image" />
        </div>
        <div className="about-me-right">
        <div className='header'>
          <h2>About me</h2>
          </div>
          
          <div className="cover-letter">

            <p>
            <p className='greetings'>Greetings,</p>I'm Valentin, an aspiring Junior Frontend Developer who recently graduated from Telerik Academy. My passion for web development has led me to acquire extensive knowledge and hands-on experience in JavaScript, React, GIT, CSS, HTML, and Chakra UI during my time at Telerik Academy.
            </p>
            <p>
            My journey of continuous learning extends to TypeScript, where I'm actively expanding my knowledge while working on personal projects to enrich my portfolio.
            </p>
            <p>
            My profound interest lies in Software Development, where I bring creativity, a strong desire to learn, and an openness to opportunities as I embark on my journey to begin a career as a Software Developer.
            </p>
          </div>
          <div className="menu">
            <ul>
              <li
                className={activeMenu === "tech" ? "active" : ""}
                onClick={() => setActiveMenu("tech")}
              >
                Tech Stack
              </li>
              <li
                className={activeMenu === "hobbies" ? "active" : ""}
                onClick={() => setActiveMenu("hobbies")}
              >
                Hobbies
              </li>
              <li
                className={activeMenu === "quote" ? "active" : ""}
                onClick={() => setActiveMenu("quote")}
              >
                Quote
              </li>
            </ul>
          </div>
          {activeMenu === "tech" && (
            <div className="tech-stack">
              <div className="tech-icons">
                <div className="tech-icon-container">
                  <FaReact className="tech-icon" />
                  <p>React</p>
                </div>
                <div className="tech-icon-container">
                  <FaJs className="tech-icon" />
                  <p>JavaScript</p>
                </div>
                <div className="tech-icon-container">
                  <FaHtml5 className="tech-icon" />
                  <p>HTML5</p>
                </div>
                <div className="tech-icon-container">
                  <FaCss3 className="tech-icon" />
                  <p>CSS3</p>
                </div>
                <div className="tech-icon-container">
                  <SiChakraui className="tech-icon" />
                  <p>ChakraUI</p>
                </div>
                <div className="tech-icon-container">
                  <FaGitlab className="tech-icon" />
                  <p>Gitlab</p>
                </div>
                <div className="tech-icon-container">
                  <TbBrandTypescript className="tech-icon" />
                  <p>TypeScript</p>
                </div>
              </div>
            </div>
          )}
          {activeMenu === "hobbies" && (
            <div className="hobbies">
              <ul>
                <li><FaDumbbell /> Fitness</li>
                <li><FaMountain /> Hiking</li>
                <li><FaSuitcase /> Traveling</li>
                <li><FaFutbol /> Football</li>
              </ul>
            </div>
          )}
          {activeMenu === "quote" && (
            <div className="life-philosophy">
              <p>"The pathway to your greatest potential is to take steps right through your greatest fears"<br />- Craig Groeschel</p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default About;