import { FaLinkedin, FaGitlab } from 'react-icons/fa';
import './Home.css';
import valentinImg from '../../assets/images/pic.png';

function Home() {
  return (
    <>
    <div className="home-container">
      <div className="left-section">
        <div id='home-header'>
          <div className='overlay'></div>
        <div className="hello-world"><div className="wrapper">
		<div id="H" className="letter">H</div>
		<div className="shadow">H</div>
	</div>
	<div className="wrapper">
		<div id="E" className="letter">E</div>
		<div className="shadow">E</div>
	</div>
	<div className="wrapper">
		<div id="L" className="letter">L</div>
		<div className="shadow">L</div>
	</div>
	<div className="wrapper">
		<div id="L" className="letter">L</div>
		<div className="shadow">L</div>
	</div>
	<div className="wrapper">
		<div id="O" className="letter">O</div>
		<div className="shadow">O</div>
	</div>
	<div className="wrapper">
		<div id="space" className="space"> </div>
		<div className="shadow"></div>
	</div>
	<div className="wrapper">
		<div id="W" className="letter">W</div>
		<div className="shadow">W</div>
	</div>
	<div className="wrapper">
		<div id="O" className="letter">O</div>
		<div className="shadow">O</div>
	</div>
	<div className="wrapper">
		<div id="R" className="letter">R</div>
		<div className="shadow">R</div>
	</div>
  <div className="wrapper">
		<div id="L" className="letter">L</div>
		<div className="shadow">L</div>
	</div>
  <div className="wrapper">
		<div id="D" className="letter">D</div>
		<div className="shadow">D</div>
	</div>
  <div className="wrapper">
		<div id="!" className="letter">!</div>
		<div className="shadow">!</div>
	</div>
  </div>
          <div className="name">
            I'm, Valentin Georgiev
          </div>
        </div>
        <div id='position'>
          Junior Frontend Developer
        </div>
        <div className="social-links">
          <a href="https://www.linkedin.com/in/valentin-georgiev-414950160/" target="_blank" rel="noopener noreferrer">
            <FaLinkedin className="social-icon" />
          </a>
          <a href="https://gitlab.com/valgeorg/" target="_blank" rel="noopener noreferrer">
            <FaGitlab className="gitlab-icon" />
          </a>
        </div>
      </div>

      <div className="right-section">
        <div className="image-container">
          <img src={valentinImg} alt='valentin-image'/>
        </div>
      </div>
    </div>
    </>
  );
}

export default Home;
