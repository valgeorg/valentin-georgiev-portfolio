import './Projects.css';
import MeteoMate from '../../assets/images/MeteoMate.png';
import Energize2 from '../../assets/images/Energize2.png';
import ComingSoon from '../../assets/images/coming-soon.jpg';
import {RiExternalLinkFill} from 'react-icons/ri'
import {FaGitlab } from 'react-icons/fa';
import { useState } from 'react';

function Projects() {
  const [visibleProjects, setVisibleProjects] = useState(2);

  const projects = [
    { name: "Energize Fitness Tracker", image: Energize2, url: "https://gitlab.com/valgeorg/fitness-tracker-app", repoUrl: "https://gitlab.com/valgeorg/fitness-tracker-repo", description: "This is a fitness tracker app that allows users to track their workouts, set goals for themselves and participate in the Energize Conquest. It includes features for managing workouts, goals, friends, and a community." },
    { name: "MeteoMate Weather", image: MeteoMate, url: "project_url_2", repoUrl: "repo_url_2", description: "MeteoMate is a weather application that allows you to search for a specific location and retrieve weather information such as temperature, feels-like temperature, weather conditions, and 3-day forecast." },
    { name: "Clothes 4 you", image: ComingSoon, url: "project_url_3", repoUrl: "repo_url_3", description: "" }
  ];

  const showMoreProjects = () => {
    setVisibleProjects(visibleProjects + 2);
  };

  return (
    <div className="projects-container">
      <h2>Projects</h2>
      <div className="project-grid">
        {projects.slice(0, visibleProjects).map((project, index) => (
          <div className="project-box shadow" key={index}>
            <a href={project.url} target="_blank" rel="noopener noreferrer">
              <img src={project.image} alt={`Project ${index + 1} Screenshot`} />
              <div className="project-details">
                <div className="project-name">{project.name}</div>
                <div className='project-description'>{project.description}</div>
                <div className="project-buttons">
                  <a href={project.url} target="_blank" rel="noopener noreferrer">Go to Project <RiExternalLinkFill/></a>
                  <span className="divider"></span>
                  <a href={project.repoUrl} target="_blank" rel="noopener noreferrer">Gitlab <FaGitlab/> </a>
                </div>
              </div>
            </a>
          </div>
        ))}
      </div>
      {visibleProjects < projects.length && (
        <button className="see-more-button" onClick={showMoreProjects}>See More</button>
      )}
    </div>
  );
}

export default Projects;
