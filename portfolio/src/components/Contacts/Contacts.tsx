import { FaEnvelope, FaPhone, FaLinkedin } from 'react-icons/fa';
import './Contacts.css'
import CV from '../../assets/CV/ValentinGeorgievCV.pdf'; 
import contactAvatar from '../../assets/images/contact-img.png'

function Contacts() {
  const handleDownloadCV = () => {

    const link = document.createElement('a');
    link.href = CV;
    link.target = '_blank';
    link.download = 'Valentin_Georgiev_CV.pdf'; 
    link.click();

  };

  return (
    <>
      <header>
      <h1>Contact me <img src={contactAvatar} alt="Contact Avatar" className='avatar'/></h1>
      </header>
      <div className="contact-info">
        <div className='email-phone'>
        <p><FaEnvelope /> valentingg1997@gmail.com</p>
        <p><FaPhone /> +359898638967</p>
        </div>
        <p></p><a href="https://www.linkedin.com/in/valentin-georgiev-414950160/" target="_blank" rel="noopener noreferrer">
            <FaLinkedin className="social-icon" />
          </a>
          <p/>
        <button onClick={handleDownloadCV}>Download CV</button> 
      </div>
    </>
  );
}

export default Contacts;