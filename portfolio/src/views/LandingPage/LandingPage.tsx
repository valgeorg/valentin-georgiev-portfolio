import Navigation from "../../Layout/Navigation/Navigation";
import Home from "../../components/Home/Home";
import About from "../../components/About/About";
import Projects from "../../components/Projects/Projects";
import Contacts from "../../components/Contacts/Contacts";
import Footer from "../../components/Footer/Footer";

const LandingPage = () => {
  return (
    <>
      <Navigation />
      <div id="home">
        <Home />
      </div>
      <div id="about">
        <About />
      </div>
      <div id="projects">
        <Projects />
      </div>
      <div id="contacts">
        <Contacts />
      </div>
      <Footer />
    </>
  );
};

export default LandingPage;
