import { Link } from "react-scroll";
import './Navigation.css';

function Navigation() {
  return (
    <nav>
      <div className="nav-container">
        <div className="logo-container">
          <div id="vg-portfolio">VG.</div>
        </div>
        <ul className="nav-links">
          <li>
            <Link to="home" className="link-one" spy={true} smooth={true}>Home</Link>
          </li>
          <li>
            <Link to="about"  className="link-two" spy={true} smooth={true}>About</Link>
          </li>
          <li>
            <Link to="projects" className="link-three" spy={true} smooth={true}>Projects</Link>
          </li>
          <li>
            <Link to="contacts" className="link-four" spy={true} smooth={true}>Contacts</Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navigation;
